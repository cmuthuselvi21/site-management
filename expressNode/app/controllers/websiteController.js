const getAccountsDetails = async (req, res) => {
  await Account.findAll({
    attributes: ["id", "name"],
  })
    .then((response) => {
      return res.send({ accounts: response });
    })
    .catch((err) => {
      return res.status(500).send({ message: err.message });
    });
};
module.exports.getAccountsDetails = getAccountsDetails;

const getWebsiteDetails = async (req, res) => {
  await Website.findAll()
    .then((response) => {
      return res.send({ websites: response });
    })
    .catch((err) => {
      return res.status(500).send({ message: err.message });
    });
};
module.exports.getWebsiteDetails = getWebsiteDetails;

const createWebsite = async (req, res) => {
  await Website.create(req.body)
    .then((response) => {
      return res.send({ response: response });
    })
    .catch((err) => {
      return res.status(500).send({ message: err.message });
    });
};
module.exports.createWebsite = createWebsite;

const updateWebsite = async (req, res) => {
  await Website.update(req.body, {
    where: {
      id: req.body.id,
    },
  })
    .then((response) => {
      return res.send({ updatedData: response });
    })
    .catch((err) => {
      return res.status(500).send({ message: err.message });
    });
};
module.exports.updateWebsite = updateWebsite;

const deleteWebsite = async (req, res) => {
  await Website.destroy({
    where: {
      id: req.body.id,
    },
  })
    .then((response) => {
      return res.send({ deletedWebsite: response });
    })
    .catch((err) => {
      return res.status(500).send({ message: err.message });
    });
};
module.exports.deleteWebsite = deleteWebsite;

const getDashboardCountData = async (req, res) => {
  let data = {};
  await Account.count()
    .then((response) => (data["accountCount"] = response))
    .catch((err) => res.status(500).send({ message: err.message }));
  await Website.findAll({
    attributes: ["title", "accountIds"],
  })
    .then((response) => {
      data["websiteCount"] = response.length;
      let topWebsite = response[0];
      response.forEach((i) => {
        if (i.accountIds) {
          if (i.accountIds.length > topWebsite.accountIds.length) {
            topWebsite = i;
          }
        }
      });
      data["topWebsite"] = topWebsite;
    })
    .catch((err) => res.status(500).send({ message: err.message }));
  return res.send({ count: data });
};
module.exports.getDashboardCountData = getDashboardCountData;
