const bcrypt = require("bcrypt");
const saltRounds = 9;

const signIn = async (req, res) => {
  const userData = await User.findOne({
    where: {
      username: req.body.username,
    },
  });
  if (userData) {
    const comparison = await bcrypt.compare(
      req.body.password,
      userData.password
    );

    if (comparison) {
      delete userData.dataValues.password;
      const token = jwt.sign({ id: userData.id }, accessTokenSecret, {
        expiresIn: 40,
      });
      userData.dataValues.token = token;
      return res.send({ user: userData });
    } else {
      return res.send({ error: "Username and Password does not match" });
    }
  } else {
    return res.send({ error: "Username does not exists" });
  }
};
module.exports.signIn = signIn;

const signUp = async (req, res) => {
  const password = req.body.password;

  //   const encryptedPassword = await bcrypt.hash(password, saltRounds);
  const salt = await bcrypt.genSalt(10);
  const encryptedPassword = await bcrypt.hash(password, salt);
  await User.create({
    username: req.body.username,
    password: encryptedPassword,
  })
    .then((response) => {
      return res.send({ user: response });
    })
    .catch((err) => res.status(500).send({ message: err.message }));
};
module.exports.signUp = signUp;
