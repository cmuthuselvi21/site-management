var express = require("express");
var router = express.Router();
const WebsiteController = require("../controllers/websiteController");
const UserAccountController = require("../controllers/userAccountController");
const authenticateJWT = require("../middleware.js");
router.get(
  "/getAccountsDetails",
  authenticateJWT.authenticateJWT,
  WebsiteController.getAccountsDetails
);
router.get(
  "/getWebsiteDetails",
  authenticateJWT.authenticateJWT,
  WebsiteController.getWebsiteDetails
);
router.post(
  "/createWebsite",
  authenticateJWT.authenticateJWT,
  WebsiteController.createWebsite
);
router.post(
  "/updateWebsite",
  authenticateJWT.authenticateJWT,
  WebsiteController.updateWebsite
);
router.post(
  "/deleteWebsite",
  authenticateJWT.authenticateJWT,
  WebsiteController.deleteWebsite
);
router.get(
  "/getDashboardCountData",
  authenticateJWT.authenticateJWT,
  WebsiteController.getDashboardCountData
);
router.post("/signIn", UserAccountController.signIn);
router.post("/signUp", UserAccountController.signUp);
module.exports = router;
