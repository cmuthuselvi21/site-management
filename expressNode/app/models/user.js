"use strict";

module.exports = (sequelize, DataTypes) => {
  var Model = sequelize.define(
    "user",
    {
      id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
      },
      username: {
        type: DataTypes.STRING(45),
        allowNull: false,
        unique: true,
      },
      password: {
        type: DataTypes.STRING(100),
        allowNull: false,
      },
    },
    {
      tableName: "user",
    },
    {
      timestamps: false,
    }
  );
  return Model;
};
