'use strict';


module.exports = (sequelize, DataTypes) => {
  var Model = sequelize.define('account', {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false
    },
    secrets: {
      type: DataTypes.JSON,
    }
  }, {
      tableName: 'account'
    },
    {
      timestamps: false
    });
  return Model;
};