const dbConfig = require("../config/dbConfig.js");
const Sequelize = require("sequelize");

// var fs = require('fs');
// var path = require('path');
// var basename = path.basename(__filename);

const sequelize = new Sequelize(
  "site_management",
  "zt-user",
  "R4ys4EM2wdougyk6",
  {
    host: "zeetim-test-db.cpto9hguocry.us-east-1.rds.amazonaws.com",
    dialect: "mysql",
    operatorsAliases: false,
    define: {
      timestamps: false,
    },
    pool: {
      max: dbConfig.pool.max,
      min: dbConfig.pool.min,
      acquire: dbConfig.pool.acquire,
      idle: dbConfig.pool.idle,
    },
  }
);

const db = {};

// fs
//   .readdirSync(__dirname)
//   .filter(file => {
//     return (file.indexOf('.') !== 0) && (file !== basename) && (file.slice(-3) === '.js');
//   })
//   .forEach(file => {
//     console.log('fileeee', file);
//     var model = sequelize['import'](path.join(__dirname, file));
//     db[model.name] = model;
//   });

// Object.keys(db).forEach(modelName => {
//   if (db[modelName].associate) {
//     db[modelName].associate(db);
//   }
// });

db.account = require("./account.js")(sequelize, Sequelize);
db.website = require("./website.js")(sequelize, Sequelize);
db.user = require("./user.js")(sequelize, Sequelize);

db.Sequelize = Sequelize;
db.sequelize = sequelize;

module.exports = db;
