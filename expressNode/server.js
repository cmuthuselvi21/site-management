const express = require('express');
const cors = require('cors');
const app = express();
require('./app/config/import');

app.use(cors());

app.use(express.json());

app.use(express.urlencoded({ extended: true }));

app.use(function (req, res, next) {
  // if (req && req.headers && req.headers.authorization) {
  //   req.headers.authorization = cryptoService.decrypt(req.headers.authorization);
  // }
  // Website you wish to allow to connect
  res.setHeader('Access-Control-Allow-Origin', '*');
  // Request methods you wish to allow
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
  // Request headers you wish to allow
  res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With, content-type, Authorization, Content-Type');
  // Set to true if you need the website to include cookies in the requests sent
  // to the API (e.g. in case you use sessions)
  res.setHeader('Access-Control-Allow-Credentials', true);
  // Pass to next layer of middleware
  next();
});

const model = require("./app/models");

const v1 = require('./app/routes/v1');

app.use('/v1', v1);

model.sequelize.authenticate().then(() => {
  console.log('Connected to SQL database:');
}).catch(err => {
  console.error('Unable to connect to SQL database:', err);
});

// model.sequelize.sync();

app.get('/', (req, res) => {
  res.json({ message: 'Welcome' });
});

const port = '8082';

app.listen(port, () => {
  console.log('server is running on port', port);
});
