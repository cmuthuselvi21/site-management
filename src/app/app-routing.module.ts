import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AllWebsiteComponent } from './website/components/all-website/all-website.component';
import { NavbarComponent } from './common/components/navbar/navbar.component';
import { DashboardComponent } from './common/components/dashboard/dashboard.component';


const routes: Routes = [
  { path: '', redirectTo: '/app/dashboard', pathMatch: 'full' },
  {
    path: 'app', component: NavbarComponent, children: [
      { path: 'dashboard', component: DashboardComponent },
      { path: 'all-website', component: AllWebsiteComponent }
    ]
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
