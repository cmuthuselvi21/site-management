import { MatSnackBarConfig } from '@angular/material';

export class ConfigurationFile {
  errorSnackbarConfig: MatSnackBarConfig = {
    duration: 4000,
    verticalPosition: 'top',
    panelClass: ['custom-snack-bar', 'error-snack-bar']
  };
}
