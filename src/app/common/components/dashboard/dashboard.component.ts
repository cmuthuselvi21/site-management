/**
 * Component which is used to display the dashboard details
 */
import { Component, OnInit, OnDestroy } from '@angular/core';
import { WebsiteService } from '../../../website/services/website.service';
import { Subscription } from 'rxjs';
import { MatSnackBar } from '@angular/material';
import { ConfigurationFile } from '../../constants/configurationFile';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent extends ConfigurationFile implements OnInit, OnDestroy {
  /**
   * Variable which is used to subscribe or unsubscribe the details.
   */
  subscriptionArray: Subscription[] = [];
  /**
   * Variable which is used to store the dashboard count details.
   */
  dashboardCountData;
  /**
   * Component constructor which is used to inject the required services.
   * @param websiteService To access the functions inside the WebsiteService.
   * @param snackBar  To access the functions inside the MatSnackBar.
   */
  constructor(
    private websiteService: WebsiteService,
    private snackBar: MatSnackBar) {
    super();
  }
  /**
   * Component onInit life cycle hook.
   */
  ngOnInit() {
    this.subscriptionArray.push(this.websiteService.getDashboardCountData().subscribe((res) => {
      if (res && res['count']) {
        this.dashboardCountData = res['count'];
      }
    }, (err) => {
      if (err && err.error && err.error.message) {
        this.snackBar.open(err.error.message, 'Close', this.errorSnackbarConfig);
      }
    }));
  }
  /**
   * Component onDestroy life cycle hook.
   */
  ngOnDestroy() {
    if (this.subscriptionArray && this.subscriptionArray.length) {
      this.subscriptionArray.forEach((i) => i.unsubscribe());
    }
  }
}

