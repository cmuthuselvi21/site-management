/**
 * Component which is used to create, edit, delete and display the website details.
 */
import { Component, OnInit, ViewChild, TemplateRef, OnDestroy } from '@angular/core';
import { WebsiteService } from '../../services/website.service';
import { MatDialog, MatSnackBar } from '@angular/material';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { ConfigurationFile } from '../../constants/configurationFile';

@Component({
  selector: 'app-all-website',
  templateUrl: './all-website.component.html',
  styleUrls: ['./all-website.component.scss']
})
export class AllWebsiteComponent extends ConfigurationFile implements OnInit, OnDestroy {
  /**
   * Variable which is used to store the website details.
   */
  websites = [];
  /**
   * Variable which is used to refer the create website dialog.
   */
  @ViewChild('addWebsite', { static: true }) addWebsite: TemplateRef<any>;
  /**
   * Variable which is used to refer the delete website confirmation dialog.
   */
  @ViewChild('deleteConfirmation', { static: true }) deleteConfirmation: TemplateRef<any>;
  /**
   * Variable which is used to create reactive forms for adding websites.
   */
  websiteForm: FormGroup;
  /**
   * Variable which is used to store the account details.
   */
  accountDetails;
  /**
   * Variable which is used to subscribe or unsubscribe the details.
   */
  subscriptionArray: Subscription[] = [];
  /**
   * Variable which is used to decide whether to display the loader or not.
   */
  isLoading: boolean;
  /**
   * Component constructor which is used to inject the required services.
   * @param websiteService  To access the functions inside the WebsiteService.
   * @param dialog To access the functions inside the MatDialog.
   * @param snackBar To access the functions inside the MatSnackBar.
   */
  constructor(
    private websiteService: WebsiteService,
    private dialog: MatDialog,
    private snackBar: MatSnackBar) {
    super();
  }
  /**
   * Component onInit life cycle hook.
   */
  ngOnInit() {
    this.websiteForm = new FormGroup({
      url: new FormControl(null, Validators.required),
      title: new FormControl(null, Validators.required),
      accountIds: new FormControl(null)
    });
    this.subscriptionArray.push(this.websiteService.getWebsiteDetails().subscribe((res) => {
      if (res && res['websites']) {
        this.websites = res['websites'];
      }
    }, (err) => {
      if (err && err.error && err.error.message) {
        this.snackBar.open(err.error.message, 'Close', this.errorSnackbarConfig);
      }
    }));
    this.subscriptionArray.push(this.websiteService.getAccountDetails().subscribe((res) => {
      console.log('response', res);
      if (res && res['accounts']) {
        this.accountDetails = res['accounts'];
      }
    }, (err) => {
      if (err && err.error && err.error.message) {
        this.snackBar.open(err.error.message, 'Close', this.errorSnackbarConfig);
      }
    }));
  }
  /**
   * Method which is used to open the add website dialog for creating/editing the websites.
   * @param editData To define the selected data to edit.
   */
  openDialog(editData?: any) {
    if (editData) {
      this.websiteForm.setValue({
        url: editData.url,
        title: editData.title,
        accountIds: editData.accountIds
      });
    }
    this.dialog.open(this.addWebsite, {
      width: '450px',
      autoFocus: false,
      disableClose: true,
      data: editData
    });
  }
  /**
   * Method which is used to open the delete confirmation dialog.
   * @param id To define the selected data to delete.
   */
  openDeleteConfirmationDialog(id: number) {
    this.dialog.open(this.deleteConfirmation, {
      autoFocus: false,
      data: id,
      disableClose: true
    });
  }
  /**
   * Method which is used to create new website.
   */
  onSubmit() {
    if (this.websiteForm.valid) {
      this.isLoading = true;
      this.subscriptionArray.push(this.websiteService.createWebsite(this.websiteForm.value).subscribe((res) => {
        if (res && res['response']) {
          this.isLoading = false;
          this.snackBar.open(this.messages.CREATE_WEBSITE_SUCCESS, 'Close', this.successSnackbarConfig);
          this.dialog.closeAll();
          this.websiteForm.reset();
          this.websites.push(res['response']);
        }
      }, (err) => {
        if (err && err.error && err.error.message) {
          this.isLoading = false;
          this.snackBar.open(err.error.message, 'Close', this.errorSnackbarConfig);
          this.dialog.closeAll();
          this.websiteForm.reset();
        }
      }));
    }
  }
  /**
   * Method which is used to update the website details.
   * @param id To define the selected data id to edit.
   */
  onUpdate(id: number) {
    if (this.websiteForm.valid) {
      this.isLoading = true;
      this.websiteForm.value.id = id;
      this.subscriptionArray.push(this.websiteService.updateWebsite(this.websiteForm.value).subscribe((res) => {
        if (res && res['updatedData']) {
          this.isLoading = false;
          this.snackBar.open(this.messages.UPDATE_WEBSITE_SUCCESS, 'Close', this.successSnackbarConfig);
          this.dialog.closeAll();
          this.websites.find((i) => {
            if (i.id === id) {
              i.url = this.websiteForm.value.url;
              i.title = this.websiteForm.value.title;
              i.accountIds = this.websiteForm.value.accountIds;
              return true;
            }
          });
          this.websiteForm.reset();
        }
      }, (err) => {
        if (err && err.error && err.error.message) {
          this.isLoading = false;
          this.snackBar.open(err.error.message, 'Close', this.errorSnackbarConfig);
          this.dialog.closeAll();
          this.websiteForm.reset();
        }
      }));
    }
  }
  /**
   * Method which is used to delete the website.
   * @param id To define the selected data to delete.
   */
  deleteWebsite(id: number) {
    this.isLoading = true;
    this.subscriptionArray.push(this.websiteService.deleteWebsite({ id }).subscribe((res) => {
      if (res && res['deletedWebsite']) {
        this.isLoading = false;
        this.snackBar.open(this.messages.DELETE_WEBSITE_SUCCESS, 'Close', this.successSnackbarConfig);
        this.dialog.closeAll();
        this.websites.splice(this.websites.findIndex((i) => i.id === id), 1);
      }
    }, (err) => {
      if (err && err.error && err.error.message) {
        this.isLoading = false;
        this.snackBar.open(err.error.message, 'Close', this.errorSnackbarConfig);
        this.dialog.closeAll();
      }
    }));
  }
  /**
   * Component OnDestroy life cycle hook.
   */
  ngOnDestroy() {
    if (this.subscriptionArray && this.subscriptionArray.length) {
      this.subscriptionArray.forEach((i) => i.unsubscribe());
    }
  }
}
