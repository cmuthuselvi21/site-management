import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AllWebsiteComponent } from './components/all-website/all-website.component';
import { CoreModule } from '../common/core.module';



@NgModule({
  declarations: [AllWebsiteComponent],
  imports: [
    CommonModule,
    CoreModule
  ]
})
export class WebsiteModule { }
