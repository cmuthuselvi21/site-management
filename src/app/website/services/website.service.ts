/**
 * Service which is used to create, edit and retrieve the website and accounts related details.
 */
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class WebsiteService {
  /**
   * Variable which is used to define the url data.
   */
  apiUrl = 'http://localhost:8082/';
  /**
   * Service constructor to inject the other needed services here.
   * @param http Service to send the api request to the server.
   */
  constructor(private http: HttpClient) { }
  /**
   * Method which is used to get all account details.
   */
  getAccountDetails() {
    return this.http.get(this.apiUrl + 'v1/' + 'getAccountsDetails');
  }
  /**
   * Method which is used to get all website details.
   */
  getWebsiteDetails() {
    return this.http.get(this.apiUrl + 'v1/' + 'getWebsiteDetails');
  }
  /**
   * Method which is used to create new website.
   * @param data To define the data to create.
   */
  createWebsite(data) {
    return this.http.post(this.apiUrl + 'v1/' + 'createWebsite', data);
  }
  /**
   * Method which is used to update the existing website.
   * @param data To define the data to update.
   */
  updateWebsite(data) {
    return this.http.post(this.apiUrl + 'v1/' + 'updateWebsite', data);
  }
  /**
   * Method which is used to delete the website.
   * @param data To define the website id.
   */
  deleteWebsite(data) {
    return this.http.post(this.apiUrl + 'v1/' + 'deleteWebsite', data);
  }
  /**
   * Method which is used to get the dashboard details.
   */
  getDashboardCountData() {
    return this.http.get(this.apiUrl + 'v1/' + 'getDashboardCountData');
  }
}
