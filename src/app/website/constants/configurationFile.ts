import { MatSnackBarConfig } from '@angular/material';

export class ConfigurationFile {
  websiteAction = {
    edit: 'Edit',
    create: 'Create'
  };
  messages = {
    CREATE_WEBSITE_SUCCESS: 'Website created successfully',
    UPDATE_WEBSITE_SUCCESS: 'Webiste details are updated successfully',
    DELETE_WEBSITE_SUCCESS: 'Website deleted successfully',
    DELETE_CONFIRMATION: 'Are you sure you want to delete?'
  };
  successSnackbarConfig: MatSnackBarConfig = {
    duration: 4000,
    verticalPosition: 'top',
    panelClass: ['custom-snack-bar', 'success-snack-bar']
  };
  errorSnackbarConfig: MatSnackBarConfig = {
    duration: 4000,
    verticalPosition: 'top',
    panelClass: ['custom-snack-bar', 'error-snack-bar']
  };
}
